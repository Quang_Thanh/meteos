package quangnguyen.com.projetmeteos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static MainActivity fa;
    public static String SERVER_URL;
    public static String FORECAST_URL;
    public static String WEATHER_URL = "http://api.openweathermap.org/data/2.5/";
    public static String OPTION = "&units=metric&appid=8b62177ed538309f1fe0756026559a29";
    public static String FAVORITE = "MyFavorite";
    public static String CITY_ID = "CityID";

    SearchView txtSearchValue;
    TextView NameCity, Temperature, MinMaxTemp, Main, Humidity, Wind, Pressure;
    Meteo meteo = new Meteo();
    SharedPreferences lastSearch;
    SharedPreferences favorite;
    GPSTracker gps;
    ImageView iconWeather;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fa = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.GPS_location);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // create class object
                gps = new GPSTracker(MainActivity.this);
                // check if GPS enabled
                if(gps.canGetLocation()){
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    SERVER_URL = WEATHER_URL + "weather?lon="+longitude+"&lat="+latitude+OPTION;
                    FORECAST_URL = WEATHER_URL + "forecast?lon="+longitude+"&lat="+latitude+OPTION;
                    new LongOperation().execute(SERVER_URL);
                    new ForecastUpdate().execute(FORECAST_URL);
                }else{
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // circle image
        Resources res = getResources();
        Bitmap src = BitmapFactory.decodeResource(res, R.drawable.alienware);
        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(res, src);
        dr.setCornerRadius(Math.max(src.getWidth(), src.getHeight() / 2.0f));
        ImageView imageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        imageView.setImageDrawable(dr);

        // Shared Preference favorite location
        favorite = getSharedPreferences(FAVORITE, Context.MODE_PRIVATE);

        // couter in right side of menu
        TextView view = (TextView) navigationView.getMenu().findItem(R.id.nav_favorite).getActionView();
        if (favorite.contains(CITY_ID)) {
            if (favorite.getString(CITY_ID, "") != "") {
                String[] s = favorite.getString(CITY_ID, "").split(",");
                view.setText(s.length + "");
            }
            else view.setText("0");
        }
        else view.setText("0");

        NameCity = (TextView) findViewById(R.id.NameCity);
        Temperature = (TextView) findViewById(R.id.Temp);
        MinMaxTemp = (TextView) findViewById(R.id.MinMaxTemp);
        Main = (TextView) findViewById(R.id.Main);
        Humidity = (TextView) findViewById(R.id.Humidity);
        Wind = (TextView) findViewById(R.id.Wind);
        Pressure = (TextView) findViewById(R.id.Pressure);
        iconWeather = (ImageView) findViewById(R.id.iconweather);

        // Web service
        SERVER_URL = WEATHER_URL+"weather?q=HoChiMinh,vn"+OPTION;
        FORECAST_URL = WEATHER_URL+"forecast?q=HoChiMinh,vn"+OPTION;

        // Shared Preference last search
        lastSearch = getSharedPreferences("MyLastPlace", Context.MODE_PRIVATE);
        if (lastSearch.contains(CITY_ID))
        {
            SERVER_URL = WEATHER_URL + "weather?id="+lastSearch.getString(CITY_ID,"")+OPTION;
            FORECAST_URL = WEATHER_URL+ "forecast?id="+lastSearch.getString(CITY_ID,"")+OPTION;
        }

        new LongOperation().execute(SERVER_URL);
        new ForecastUpdate().execute(FORECAST_URL);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        txtSearchValue = (SearchView) menu.findItem(R.id.action_search).getActionView();
        txtSearchValue.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String s = txtSearchValue.getQuery().toString();
                // remove whitespace
                s = s.replaceAll(" ","");
                SERVER_URL = WEATHER_URL+"weather?q="+s+OPTION;
                FORECAST_URL = WEATHER_URL+"forecast?q="+s+OPTION;
                //recreate the 'original' ActionBar (collapse the SearchBox)
                invalidateOptionsMenu();
                //clear searchView text
                txtSearchValue.setQuery("", false);
                new LongOperation().execute(SERVER_URL);
                new ForecastUpdate().execute(FORECAST_URL);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_favorite) {
            Intent fav = new Intent(MainActivity.this,FavoriteActivity.class);
            startActivity(fav);
        } else if (id == R.id.action_refresh){
            new LongOperation().execute(SERVER_URL);
            new ForecastUpdate().execute(FORECAST_URL);
        }
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Toast.makeText(getApplicationContext(), "Click at Settings", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_about) {
            Toast.makeText(getApplicationContext(), "Click at About", Toast.LENGTH_LONG).show();
        }
        else if (id == R.id.nav_favorite){
            Intent fav = new Intent(MainActivity.this,FavoriteActivity.class);
            startActivity(fav);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private class ForecastUpdate extends AsyncTask<String, Void, Void> {
        private String jsonResponse;
        private ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        protected void onPreExecute() {
            dialog.setMessage("Please wait..");
            dialog.show();
        }

        protected Void doInBackground(String... urls) {
            try {
                // solution uses Java.Net class (Apache.HttpClient is now deprecated)
                // STEP1. Create a HttpURLConnection object releasing REQUEST to given site
                URL url = new URL(urls[0]); //argument supplied in the call to AsyncTask
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "");
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.connect();

                // STEP2. wait for incoming RESPONSE stream, place data in a buffer
                InputStream isResponse = urlConnection.getInputStream();
                BufferedReader responseBuffer = new BufferedReader(new
                        InputStreamReader(isResponse));

                // STEP3. Arriving JSON fragments are concatenate into a StringBuilder
                String myLine = "";
                StringBuilder strBuilder = new StringBuilder();
                while ((myLine = responseBuffer.readLine()) != null) {
                    strBuilder.append(myLine);
                }
                //show response (JSON encoded data)
                jsonResponse = strBuilder.toString();
                Log.e("RESPONSE", jsonResponse);
            } catch (Exception e) {
                Log.e("RESPONSE Error", e.getMessage());
            }
            return null; // needed to gracefully terminate Void method
        }

        ArrayList<Meteo> meteoList;
        protected void onPostExecute(Void unused) {
            try {
                dialog.dismiss();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
                String strDate = sdf.format(c.getTime());
                TextView Time = (TextView) findViewById(R.id.currenttime);
                Time.setText("Today : " +strDate);

                // update GUI with JSON Response
                //txtResponseJson.setText(jsonResponse);

                // Step4. Convert JSON list into a Java collection of Person objects
                // prepare to decode JSON response and create Java list
                if (jsonResponse != null) {
                    TextView[] ListTextView = new TextView[5];
                    ListTextView[0] = (TextView) findViewById(R.id.forecast0);
                    ListTextView[1] = (TextView) findViewById(R.id.forecast1);
                    ListTextView[2] = (TextView) findViewById(R.id.forecast2);
                    ListTextView[3] = (TextView) findViewById(R.id.forecast3);
                    ListTextView[4] = (TextView) findViewById(R.id.forecast4);

                    Gson gson = new Gson();
                    JsonObject JsonObject = (JsonObject) new JsonParser().parse(this.jsonResponse);
                    Type listType = new TypeToken<ArrayList<Meteo>>() {
                    }.getType();
                    meteoList = gson.fromJson(JsonObject.getAsJsonArray("list"), listType);
                    String[] List = new String[6];
                    Integer[] Position = new Integer[6];
                    List[0] = meteoList.get(0).getDt_txt().substring(0, 10);
                    int k = 0;
                    Position[k] = 0;
                    for (int i = 1; i < meteoList.size(); i++) {
                        String s = meteoList.get(i).getDt_txt().substring(0, 10);
                        if (!s.equals(List[k])) {
                            k += 1;
                            List[k] = s;
                            Position[k] = i;
                        }
                    }

                    Meteo m;
                    for (int i = 0; i < ListTextView.length; i++) {
                        if (List[5] != null)
                            m = meteoList.get(Position[i + 1]);
                        else
                            m = meteoList.get(Position[i]);
                        String date = m.getDt_txt().substring(8, 10);
                        String month = m.getDt_txt().substring(5, 7);
                        ListTextView[i].setText(date + "/" + month + " : " +
                                m.getMain().getTemp_min()+" - "+m.getMain().getTemp_max() + " °C, " + m.getWeather().get(0).getMain());
                    }
                }
            } catch (JsonSyntaxException e) {
                Log.e("POST-Execute", e.getMessage());
            }
        }


    }

    private class LongOperation extends AsyncTask<String, Void, Void> {
        private String jsonResponse;
        private ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        protected void onPreExecute() {
            dialog.setMessage("Please wait..");
            dialog.show();
        }

        protected Void doInBackground(String... urls) {
            try {
                // solution uses Java.Net class (Apache.HttpClient is now deprecated)
                // STEP1. Create a HttpURLConnection object releasing REQUEST to given site
                URL url = new URL(urls[0]); //argument supplied in the call to AsyncTask
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "");
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.connect();

                // STEP2. wait for incoming RESPONSE stream, place data in a buffer
                InputStream isResponse = urlConnection.getInputStream();
                BufferedReader responseBuffer = new BufferedReader(new
                        InputStreamReader(isResponse));

                // STEP3. Arriving JSON fragments are concatenate into a StringBuilder
                String myLine = "";
                StringBuilder strBuilder = new StringBuilder();
                while ((myLine = responseBuffer.readLine()) != null) {
                    strBuilder.append(myLine);
                }
                //show response (JSON encoded data)
                jsonResponse = strBuilder.toString();
                Log.e("RESPONSE", jsonResponse);
            } catch (Exception e) {
                Log.e("RESPONSE Error", e.getMessage());
            }
            return null; // needed to gracefully terminate Void method
        }

        protected void onPostExecute(Void unused) {
            try {
                dialog.dismiss();

                // update GUI with JSON Response
                //txtResponseJson.setText(jsonResponse);

                // Step4. Convert JSON list into a Java collection of Person objects
                // prepare to decode JSON response and create Java list
                if(jsonResponse != null) {
                    Gson gson = new Gson();
                    meteo = gson.fromJson(jsonResponse, Meteo.class);

                    NameCity.setText(meteo.getName() + ", " + meteo.getSys().getCountry());
                    Temperature.setText(meteo.getMain().getTemp() + "°C");
                    Main.setText( meteo.getWeather().get(0).getDescription());
                    Pressure.setText("Pressure : "+meteo.getMain().getPressure() + " hpa");
                    Humidity.setText("Humidity : " +meteo.getMain().getHumidity() + "%");
                    Wind.setText("Wind : "+meteo.getWind().getSpeed()+" m/s");
                    MinMaxTemp.setText("Temp : "+meteo.getMain().getTemp_min()+" - " + meteo.getMain().getTemp_max()+" °C");
                    Date datetime = new Date(Long.valueOf(meteo.getDateTime()) * 1000);
                    String vv = new SimpleDateFormat("MM dd, yyyy hh:mm:ss a").format(datetime);
                    boolean morning = false;
                    if (vv.indexOf("AM") > 0 || vv.indexOf("SA") > 0){
                        morning = true;
                    }
                    else if(vv.indexOf("PM") > 0 || vv.indexOf("CH") > 0){
                        morning = false;
                    }

                    String mainWeather = meteo.getWeather().get(0).getMain();
                    switch(mainWeather){
                        case "Clear":
                            if(morning) iconWeather.setImageResource(R.drawable.clearskyday);
                            else iconWeather.setImageResource(R.drawable.clearskynight);
                            break;
                        case "Clouds":
                            if(morning) iconWeather.setImageResource(R.drawable.scattedcloudday);
                            else iconWeather.setImageResource(R.drawable.scattedcloudnight);
                            break;
                        case "Drizzle":
                            iconWeather.setImageResource(R.drawable.showerrain);
                            break;
                        case "Mist":
                            iconWeather.setImageResource(R.drawable.mist);
                            break;
                        case "Rain":
                            if(morning) iconWeather.setImageResource(R.drawable.rainday);
                            else iconWeather.setImageResource(R.drawable.rainday);

                            break;
                        case "Thunderstorm":
                            iconWeather.setImageResource(R.drawable.thunderstorm);
                            break;
                        case  "Snow":
                            iconWeather.setImageResource(R.drawable.snow);
                            break;
                    }
                    // save to SharePrefs
                    SharedPreferences.Editor editor = lastSearch.edit();
                    editor.putString("CityID",meteo.getId()+"");
                    editor.commit();

                    // verifie favorite place
                    final Button add_favorite = (Button) findViewById(R.id.add_favorite);

                    boolean contenu = false;
                    if (favorite.contains(CITY_ID))
                        contenu = favorite.getString(CITY_ID,"").contains(meteo.getId()+"");

                    if (contenu)
                    {
                        add_favorite.setBackgroundResource(R.drawable.ic_favorite_white_48dp);
                        add_favorite.setTag("contenu");
                    }
                    else {
                        add_favorite.setBackgroundResource(R.drawable.ic_add_favorite);
                        add_favorite.setTag("incontenu");
                    }

                    add_favorite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences.Editor edit = favorite.edit();
                            if (add_favorite.getTag().equals("incontenu"))
                            {
                                if (favorite.contains(CITY_ID)) {
                                    if (favorite.getString(CITY_ID, "") != "") {
                                        String s = favorite.getString(CITY_ID, "") + "," + meteo.getId();
                                        edit.putString(CITY_ID, s);
                                    }
                                    else edit.putString(CITY_ID,meteo.getId()+"");
                                }
                                else edit.putString(CITY_ID,meteo.getId()+"");
                                edit.commit();
                                add_favorite.setBackgroundResource(R.drawable.ic_favorite_white_48dp);
                                add_favorite.setTag("contenu");
                            }
                            else {
                                String s = favorite.getString(CITY_ID, "");
                                s=s.replace(","+meteo.getId(),"");
                                s=s.replace(meteo.getId()+",","");
                                s=s.replace(meteo.getId()+"","");
                                edit.putString(CITY_ID, s);
                                edit.commit();
                                add_favorite.setBackgroundResource(R.drawable.ic_add_favorite);
                                add_favorite.setTag("incontenu");
                            }

                            // update couter in right side of menu
                            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                            TextView view = (TextView) navigationView.getMenu().findItem(R.id.nav_favorite).getActionView();
                            if (favorite.contains(CITY_ID)) {
                                if (favorite.getString(CITY_ID, "") != "") {
                                    String[] s = favorite.getString(CITY_ID, "").split(",");
                                    view.setText(s.length + "");
                                }
                                else view.setText("0");
                            }
                            else view.setText("0");
                        }
                    });

                    FloatingActionButton button = (FloatingActionButton) findViewById(R.id.map_location);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // map is centered aroung given Lat, Long
                            Intent intent = new Intent(MainActivity.this,MapsActivity.class);
                            intent.putExtra("longtitude",meteo.getCoord().getLon());
                            intent.putExtra("lattitude",meteo.getCoord().getLat());
                            intent.putExtra("cityName",meteo.getName());
                            startActivity(intent);
                        }
                    });

                }
            } catch (JsonSyntaxException e) {
                Log.e("POST-Execute", e.getMessage());
            }
        }


    }

}
