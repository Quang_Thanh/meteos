package quangnguyen.com.projetmeteos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class FavoriteActivity extends AppCompatActivity {
    String SERVER_URL;
    String WEATHER_URL = "http://api.openweathermap.org/data/2.5/group?id=";
    String OPTION = "&units=metric&appid=8b62177ed538309f1fe0756026559a29";
    String FAVORITE = "MyFavorite";
    String CITY_ID = "CityID";
    SharedPreferences listFavorite;
    ArrayList<Meteo> meteoList;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        listFavorite = getSharedPreferences(FAVORITE, Context.MODE_PRIVATE);
        if (listFavorite.contains(CITY_ID)) {
            SERVER_URL = WEATHER_URL+listFavorite.getString(CITY_ID, "")+OPTION;
            new LongOperation().execute(SERVER_URL);
        }
    }

    private class LongOperation extends AsyncTask<String, Void, Void> {
        private String jsonResponse;
        private ProgressDialog dialog = new ProgressDialog(FavoriteActivity.this);

        protected void onPreExecute() {
            dialog.setMessage("Please wait..");
            dialog.show();
        }

        protected Void doInBackground(String... urls) {
            try {
                // solution uses Java.Net class (Apache.HttpClient is now deprecated)
                // STEP1. Create a HttpURLConnection object releasing REQUEST to given site
                URL url = new URL(urls[0]); //argument supplied in the call to AsyncTask
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "");
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.connect();

                // STEP2. wait for incoming RESPONSE stream, place data in a buffer
                InputStream isResponse = urlConnection.getInputStream();
                BufferedReader responseBuffer = new BufferedReader(new
                        InputStreamReader(isResponse));

                // STEP3. Arriving JSON fragments are concatenate into a StringBuilder
                String myLine = "";
                StringBuilder strBuilder = new StringBuilder();
                while ((myLine = responseBuffer.readLine()) != null) {
                    strBuilder.append(myLine);
                }
                //show response (JSON encoded data)
                jsonResponse = strBuilder.toString();
                Log.e("RESPONSE", jsonResponse);
            } catch (Exception e) {
                Log.e("RESPONSE Error", e.getMessage());
            }
            return null; // needed to gracefully terminate Void method
        }

        protected void onPostExecute(Void unused) {
            try {
                dialog.dismiss();

                // update GUI with JSON Response
                //txtResponseJson.setText(jsonResponse);

                // Step4. Convert JSON list into a Java collection of Person objects
                // prepare to decode JSON response and create Java list
                if (jsonResponse != null) {
                    listView = (ListView) findViewById(R.id.listView);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            // to get position of favorite location in ShareReference
                            SharedPreferences favorite = getSharedPreferences(FAVORITE, Context.MODE_PRIVATE);
                            String[] ListFavorite = favorite.getString(CITY_ID,"").split(",");

                            // to get ID of favorite location clicked and update to last search location
                            SharedPreferences lastSearch = getSharedPreferences("MyLastPlace", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = lastSearch.edit();
                            editor.putString(CITY_ID,ListFavorite[position]+"");
                            editor.commit();

                            // end current activity
                            MainActivity.fa.finish();
                            // start new activity of favorite location
                            startActivity(new Intent(FavoriteActivity.this,MainActivity.class));
                            // finish this activity
                            finish();
                        }
                    });

                    Gson gson = new Gson();
                    JsonObject JsonObject = (JsonObject)new JsonParser().parse(this.jsonResponse);
                    Type listType = new TypeToken<ArrayList<Meteo>>(){}.getType();
                    meteoList = gson.fromJson(JsonObject.getAsJsonArray("list"), listType);
                    listView.setAdapter(new MeteoAdapter(FavoriteActivity.this, meteoList));
                }

            } catch (JsonSyntaxException e) {
                Log.e("POST-Execute", e.getMessage());
            }
        }


    }
}
