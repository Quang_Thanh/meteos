package quangnguyen.com.projetmeteos;

import java.util.ArrayList;

/**
 * Created by Quang Nguyen on 23/05/2016.
 */
public class Meteo {
    private int id;
    private String name;
    private String dt;
    private ArrayList<Weather> weather;
    private Coord coord;
    private Main main;
    private Wind wind;
    private Sys sys;

    public String getDt_txt() {
        return dt_txt;
    }

    private String dt_txt;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDateTime() { return dt;}

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public Coord getCoord() {
        return coord;
    }

    public Main getMain() {
        return main;
    }

    public Sys getSys() {
        return sys;
    }

    public Wind getWind() {return wind;}
}
class Wind {
    private String speed;

    public String getSpeed() {
        return speed;
    }
}
 class Weather {
    public String getMain() {
        return main;
    }

    String main;

    public String getDescription() {
        return description;
    }

    String description;

}

 class Coord {
    public Float getLon() {
        return lon;
    }

    public Float getLat() {
        return lat;
    }

   Float lon, lat;
}

 class Main {
    String temp,pressure,humidity,temp_min,temp_max;

     public String getTemp_max() {
         return temp_max;
     }

     public String getTemp_min() {
         return temp_min;
     }

     public String getTemp() {
        return temp;
    }

    public String getPressure() {
        return pressure;
    }

    public String getHumidity() {
        return humidity;
    }
}

 class Sys {
    String country;

    public String getCountry() {
        return country;
    }
}
