package quangnguyen.com.projetmeteos;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Thanh Nguyen on 18-Jun-16.
 */
public class MeteoAdapter extends BaseAdapter{

    public ArrayList<Meteo> meteoList;
    private LayoutInflater inflater;

    public MeteoAdapter(Context context, ArrayList<Meteo> meteoList){
        this.meteoList = meteoList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return meteoList.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    private class Holder
    {
        private TextView MaxMin;
        private TextView City;
        private TextView Temp;
        private TextView Weather;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();

        // Get view of item
        View rowView = this.inflater.inflate(R.layout.item_view, null);
        holder.MaxMin = (TextView) rowView.findViewById(R.id.MinMaxTemp);
        holder.City = (TextView) rowView.findViewById(R.id.City);
        holder.Temp = (TextView) rowView.findViewById(R.id.Temperature);
        holder.Weather = (TextView) rowView.findViewById(R.id.Weather);

        // Fill contents
        holder.MaxMin.setText("Temp: " +this.meteoList.get(position).getMain().getTemp_min()+"-"+this.meteoList.get(position).getMain().getTemp_max()+"°C" );
        holder.City.setText(this.meteoList.get(position).getName());
        holder.Temp.setText(this.meteoList.get(position).getMain().getTemp()+"°C");
        holder.Weather.setText(this.meteoList.get(position).getWeather().get(0).getMain());
        return rowView;
    }


}
